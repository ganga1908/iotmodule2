## IIoT Protocols

### 1. 4-20 mA

The 4-20 mA loop is used to transmit process information in the form of analog signals, where varying amounts of current and voltage are used to transmit information.

This provides process control based on the results of process monitoring, leading to active changing of the process.
#### How does it work?

Consider a DC current loop, there is a voltage drop/gain across every element but the `current remains the same` throughout the loop.

This is the critical principle of the 4-20 loop. This makes current a very reliable means of conveying process information.

#### Components of 4-20 mA current loop
![Components](/extras/protocols/components.png)

 **1. Sensor :** Measures some process variable like temperature, humidity, flow, light or pressure.

 **2. Transmitter :** Converts the quantity being measured into an electrical signal in the range of 4 mA to 20 mA.

 **3. Power Source :** A power source to produce the DC current. Typical voltages used for this are 9V, 12V, 24V etc,. Care is taken to make sure that the source voltage is at least 10% greater than the voltage rop across elements.

 **4. Loop :** A wire carrying current connecting the device to be controlled and the transmitter. This doesn't result in a significant voltage drop unless it is drawn over long distances.

 **5. Receiver :** This device interprets the electrical signal at the other end, analyses it and sometimes even displays it. Digital displays, controllers, actuators, and valves are common devices incorporated into the loop.


### 2. Modbus
![modbus](/extras/protocols/modbus.png)

Modbus is a communication protocol used to transmit signals from instrumentation and control devices(PLCs) back to a main controller, or data gathering system.

#### How does it work?

 - The device requesting information is called “master” and “slaves” are the devices supplying information.
 - In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.
 - Communication between a master and a slave occurs in a frame that indicates a function code.
 - The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
 - The slave then responds, based on the function code received.

The protocol is commonly used in IoT as a local interface to manage devices.

Modbus protocol can be used over 2 interfaces:
 - RS485 - called as Modbus RTU
 - Ethernet - called as Modbus TCP/IP

**What is RS485?**

![Modbus RTU](/extras/protocols/modbus_rtu.png)

RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.

RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. Mainly done through an easy-to-use USB.

**What is Ethernet?**

![Modbus TCP/IP](/extras/protocols/modbus_tcp.png)

Ethernet can be used to connect stationary or fixed IoT devices.

It has very low latency which makes it suitable for mission critical IoT applications.

### 3. OPC UA

![Summary](/extras/protocols/opcua_summary.png)

OPC Unified Architecture(OPCUA) is a machine to machine communication protocol for Industrial Automation developed by the OPC Foundation.

It's multi-layered approach accomplishes the original design specification goals of:
 - Functional equivalence, all COM OPC Classic specifications are mapped to UA
 - `Platform independence` from an embedded micro-controller to cloud-based infrastructure
 - Secure encryption, authentication, and auditing
 - Extensible ability to `add new features` without affecting existing applications
 - Comprehensive information modeling for defining complex information

#### OPC UA Client vs. Server

OPC UA is composed of a client and a server. The client device requests information. The server device provides it. 

An OPC UA server models data, information, processes, and systems as objects and presents those objects to clients in ways that are useful to vastly different types of client applications.

![Client](/extras/protocols/opcua_client.png)
![Server](/extras/protocols/opcua_server.png)

## Cloud Protocols

### 1. MQTT - Message Queuing Telemetry Transport

 - A simple messaging protocol for constrained devices with low bandwidth.
 - A lightweight publish-and-subscribe system to publish and receive messages as a client.
 - It is the standard messaging and data exchange protocol for IoT.

Communication between several devices can be established simultaneously.
#### Overview

![MQTT Overview](/extras/protocols/mqtt_overview.png)

 - `MQTT Client` as a publisher sends a message to the `MQTT Broker`.
 - The broker distributes the message accordingly to all other MQTT clients subscribed to the topic on which the publisher publishes the message.
 - `Topics` are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.

### 2. HTTP - Hyper Text Transfer Protocol

 - It is a request-response protocol.
 - The client sends an HTTP request.
 - The server returns an HTTP response.

#### The Request

![HTTP](/extras/protocols/http.png)

It has 3 parts:
 - Request line
 - HTTP headers
 - Message body                      

````
GET / HTTP/1.1
Host: xkcd.com
Accept: text/html
User-Agent: Mozilla/5.0 (Macintosh)  
````
There are different types of methods:

**GET** : Retrieve the resource from the server (e.g. when visiting a page)

**POST** : Create a resource on the server (e.g. when submitting a form)

**PUT/PATCH** : Update the resource on the server (used by APIs)

**DELETE** : Delete the resource from the server (used by APIs)

#### The Response

It comprises of 3 parts:
 - Status line
 - HTTP header
 - Message body

```
HTTP/1.1 200 OK
Date: Sat, 02 Apr 2011 21:05:05 GMT
Server: lighttpd/1.4.19
Content-Type: text/html
<html>
    <!-- ... HTML for the xkcd comic -->
</html>
```

The server sends different status codes depending on the situation,

Few are listed below.

![Status Codes](/extras/protocols/http_status.jpg)

***All of the above protocols has its pros and cons which should be examined carefully before making a choice.***



